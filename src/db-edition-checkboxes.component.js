/**
 * Created by richard on 19/10/16.
 */

/*global angular */
'use strict';

//noinspection JSUnusedGlobalSymbols
angular.module('bitcraft-db-edition-checkboxes')
    .component('bitcraftDbEditionCheckboxes', {
        templateUrl: './js/db-edition-checkboxes/db-edition-checkboxes.template.html', // this line will be replaced
        bindings: {
            value: '=',
            edit: '<'
        }
    });
